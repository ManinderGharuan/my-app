import React, { Component } from 'react';
import {
    Route,
    BrowserRouter as Router
} from 'react-router-dom';
import './App.css';
import NavigationMenu from '../NavigationMenu';
import ShowOrgs from '../ShowOrgs';
import Form from '../Form';

class App extends Component {
    render() {
        return (
           <Router>
             <div className="App">
             <NavigationMenu />

                <div className="content">
                   <Route exact path="/" component={ShowOrgs}/>
                   <Route path="/orgs" component={Form}/>
                </div>
             </div>
           </Router>
        );
    }
}

export default App;
