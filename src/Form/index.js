import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addCompany } from '../redux/actions';
import './Form.css';

const mapDispatchToProps = dispatch => {
    return {
        addCompany: company => dispatch(addCompany(company))
    };
};

class ConnectedForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      description: "",
      type: "it_services",
      website: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const {name} = event.target;
    const {value} = event.target;

    this.setState({ [name]: value });
  }

  handleSubmit(event) {
    event.preventDefault();

    this.props.addCompany(this.state);

    this.setState({
      name: "",
      description: "",
      type: "it_services",
      website: ""
    });
  }

  render() {
     return (
      <div className='organization'>
        <h1 className='org-head'>Add Company</h1>

        <form className='org-form' onSubmit={this.handleSubmit}>
          <input type='text' className='org-input name' placeholder='Name'
            name="name" value={this.state.name} onChange={this.handleChange} />

          <textarea className='org-input Description' placeholder='Description'
            name="description" value={this.state.description} onChange={this.handleChange} />

          <select className="org-input" name="type" value={this.state.type}
            onChange={this.handleChange} >
            <option value="it_services">IT Services</option>
            <option value="hardware">Other</option>
          </select>

          <input type='text' className='org-input website' placeholder='Website'
            name="website" value={this.state.website} onChange={this.handleChange} />

          <input className="submit-input" type="submit" value="Submit" />
        </form>
      </div>
     );
  }
}

const Form = connect(null, mapDispatchToProps)(ConnectedForm);

export default Form;
