import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './Navigation.css';

class NavigationMenu extends Component {
    render() {
        return (
           <div className="navigation-bar">
              <div className="wrapper">
                <NavLink to="/">
                  <h1>My App</h1>
                </NavLink>

                <NavLink to="/orgs">
                  <span>Add</span>
                </NavLink>
              </div>
           </div>
        );
    }
}

export default NavigationMenu;
