import React, { Component } from 'react';
import { connect } from 'react-redux';

const mapStateToProps = state => {
    return { orgs: state.companies };
};

class ConnectedShowOrgs extends Component {
  renderRow(org) {
    return (
      <div className="row">
        <div className="row-item" >
          <div className="row-inner-item show-name">
            <span>Name: {org.name}</span>
          </div>

          <div className="row-inner-item show-discription">
            <span>Description: {org.discription}</span>
          </div>

          <div className="row-inner-item type">
            <span>Type: {org.type}</span>
          </div>

          <div className="row-inner-item website">
            <span>Website: {org.website}</span>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { orgs } = this.props;

    return (
      <div className="org-list-container">
        {orgs.map( org => this.renderRow(org) )}
      </div>
    );
  }
}

const ShowOrgs = connect(mapStateToProps)(ConnectedShowOrgs);

export default ShowOrgs;
