import { ADD_COMPANY } from '../constants/action-types';

export const addCompany = company => ({type: ADD_COMPANY, payload: company });
