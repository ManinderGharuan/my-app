import { ADD_COMPANY } from '../constants/action-types';

const companyReducer = (state = [], action) => {
    switch (action.type) {
    case ADD_COMPANY:
        return [...state, action.payload];
    default:
        return state;
    }
};

export default companyReducer;
